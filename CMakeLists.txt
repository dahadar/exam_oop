cmake_minimum_required(VERSION 3.10)
project(exam_oop)

set(CMAKE_CXX_STANDARD 14)

add_executable(exam_oop main.cpp test/test.cpp title.h)