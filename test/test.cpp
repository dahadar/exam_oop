//
// Created by Daha on 15.06.2021.
//
#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include "../catch.hpp"
#include "../title.h"

TEST_CASE() {
REQUIRE( test_f(1) == 0 );
REQUIRE( test_f(2) == 0 );
REQUIRE( test_f(8) == 8 );
REQUIRE( test_f(10) == 10 );
}

